# List installed jdks
```bash
/usr/libexec/java_home -V
```
# Manual instalation

### Download OpenJDK 14 for Mac

*  Go to https://jdk.java.net/archive/

* move to ~/Downloads/

    ```bash
    cd ~/Downloads/
    ```
* Put the archive in that location

### Extract the download archive
```bash
tar -xf openjdk-14.0.2_osx-x64_bin.tar
```

### Uninstall existing Oracle JDK
```bash
sudo rm -rf "/Library/Java/JavaVirtualMachines/JDK_VERSION.jdk"
```

### Install JDK
```bash
sudo mv jdk-14.0.2.jdk /Library/Java/JavaVirtualMachines
```

### Check installed JDKs
```bash
/usr/libexec/java_home -V
```

# Homebrew Install 

### Install Homebrew and update it
```bash
brew update
```

### Install openjdk8
```bash
brew cask install adoptopenjdk8
```

### Add adoptopenjdk/openjdk.
```bash
brew tap adoptopenjdk/openjdk
```

### Find all available JDK.
```bash
/usr/libexec/java_home -V
```

# Switch between JDK

* Put the following line in your **.bash_profile** (create it if not exist) 

    ```bash
    alias j8="export JAVA_HOME=`/usr/libexec/java_home -v 1.8`; java -version"
    alias j14="export JAVA_HOME=`/usr/libexec/java_home -v 14.0.2`; java -version"
    alias j15="export JAVA_HOME=`/usr/libexec/java_home -v 15`; java -version"
    ```
* Add the folowing line in your **.zshrc**

    ```bash
    source ~/.bash_profile
    ```

* To switch bewteen version, you have just to run on of the following commands in your terminal

    ```bash
    j8
    # OR
    j14
    # OR
    j15
    ```