# Install JDK
```bash
sudo apt install openjdk-14-jdk
```

# Check Java version
```bash
java --version 
```

# List installed JDKs
```
sudo update-alternatives --config java
```

# Setup environnment Variable JAVA_HOME

### Find the Java installation path and copy it
```bash
sudo update-alternatives --config java
```

### Update /etc/environnment file
```bash
# Cat /etc/environnment file
cat /etc/environnment

# Add this line to the end of the file
JAVA_HOME="/usr/lib/jvm/java-14-openjdk-amd64"

# Execute the following command to source the file
source /etc/environnment
```

